package com.Contour.ProfileManager.models;

public class Member {
	String firstName;
	String lastName;
	String title;
	String company;
	String phoneNumber;
	String websiteURL;
	String emailAddress;

	public Member(String firstName, String lastName, String title, String company) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.title = title;
		this.company = company;
	}

	public Member(String firstName, String lastName, String title, String company, String phoneNumber, String websiteURL, String emailAddress) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.title = title;
		this.company = company;
		this.phoneNumber = phoneNumber;
		this.websiteURL = websiteURL;
		this.emailAddress = emailAddress;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getWebsiteURL() {
		return websiteURL;
	}

	public void setWebsiteURL(String websiteURL) {
		this.websiteURL = websiteURL;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
}
