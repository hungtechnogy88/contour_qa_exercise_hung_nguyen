package com.Contour.ProfileManager.pages;

import com.Contour.ProfileManager.models.Member;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.logging.Logger;

public class HomePage extends NavigateBars {
	private static final Logger LOGGER = Logger.getLogger(HomePage.class.getName());

	@FindBy(xpath = "//tbody")
	WebElement tableMembers;
	@FindAll(@FindBy(xpath = "//tbody/tr/td[1]"))
	List<WebElement> IDRows;
	@FindAll(@FindBy(xpath = "//tbody/tr/td[2]"))
	List<WebElement> memberNameRows;
	@FindAll(@FindBy(xpath = "//tbody/tr/td[3]"))
	List<WebElement> titleRows;
	@FindAll(@FindBy(xpath = "//tbody/tr/td[4]"))
	List<WebElement> companyRows;
	@FindAll(@FindBy(xpath = "//tbody/tr/td[5]"))
	List<WebElement> emailRows;

	public HomePage(WebDriver driver) {
		super(driver);
	}

	@Step("Verify if a user is displaying in the table on the Home page")
	public boolean isDisplayingUser(String userID, Member member){
		for(int i = 0; i < IDRows.size(); i++){
			if(IDRows.get(i).getText().trim().equals(userID.trim())){
				LOGGER.info("Found member at: " + userID);
				// Verify the added member info on this row
				if(memberNameRows.get(i).getText().trim().equals(member.getFirstName() + " " + member.getLastName())
					&& titleRows.get(i).getText().trim().equals(member.getTitle())
					&& companyRows.get(i).getText().trim().equals(member.getCompany())
					&& emailRows.get(i).getText().trim().equals(member.getEmailAddress())){
					LOGGER.info("All data of the newly added member are displaying correctly on the Home page!");
					return true;
				}
			}
		}
		LOGGER.info("Not found member with ID: " +userID);
		return false;
	}

	@Step("Home Page is loaded")
	public boolean pageIsLoaded() {
		waitElementVisibility(tableMembers, 3);
		return tableMembers.isDisplayed();
	}
}
