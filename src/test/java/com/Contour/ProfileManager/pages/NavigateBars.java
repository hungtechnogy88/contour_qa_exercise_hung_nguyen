package com.Contour.ProfileManager.pages;

import com.Contour.ProfileManager.helpers.WebElementHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.logging.Logger;

public class NavigateBars extends WebElementHelper {
    WebDriver driver;
    @FindBy(xpath = "//a[text() = 'Home']")
    WebElement homeMenu;
    @FindBy(xpath = "//a[text() = 'Add Member']")
    WebElement addMemberMenu;
    @FindBy(xpath = "//a[text() = 'Search Member']")
    WebElement searchMemberMenu;
    @FindBy(xpath = "//a[text() = 'View Member']")
    WebElement viewMemberMenu;

    public NavigateBars(WebDriver driver) {
        super(driver);
    }

    public HomePage navigateToHomePage(){
        homeMenu.click();
        return new HomePage(driver);
    }

    public AddMemberPage navigateToAddMemberPage(){
        addMemberMenu.click();
        return new AddMemberPage(driver);
    }

    public SearchMemberPage navigateToSearchMemberPage(){
        searchMemberMenu.click();
        return new SearchMemberPage(driver);
    }

    public ViewMemberPage navigateToViewMemberPage(){
        viewMemberMenu.click();
        return new ViewMemberPage(driver);
    }
}
