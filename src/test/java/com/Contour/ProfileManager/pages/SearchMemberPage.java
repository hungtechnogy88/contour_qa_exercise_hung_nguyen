package com.Contour.ProfileManager.pages;

import com.Contour.ProfileManager.models.Member;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.logging.Logger;

public class SearchMemberPage extends NavigateBars {
	private static final Logger LOGGER = Logger.getLogger(SearchMemberPage.class.getName());

	@FindBy(id = "default-search")
	WebElement searchTxt;
	@FindBy(xpath = "//button[@type= \"submit\"]")
	WebElement searchButton;
	@FindBy(xpath = "//tbody/tr/td[1]")
	WebElement IDField;
	@FindBy(xpath = "//tbody/tr/td[2]")
	WebElement memberNameField;
	@FindBy(xpath = "//tbody/tr/td[3]")
	WebElement titleField;
	@FindBy(xpath = "//tbody/tr/td[4]")
	WebElement companyField;
	@FindBy(xpath = "//tbody/tr/td[5]")
	WebElement emailField;

	public SearchMemberPage(WebDriver driver) {
		super(driver);
	}

	@Step("Input Searching value")
	public void inputSearchValue(String searchValue) {
		searchTxt.clear();
		searchTxt.sendKeys(searchValue);
	}

	@Step("Click Search button")
	public void clickSearchButton() {
		searchButton.click();
	}

	@Step("Search a member")
	public void searchAMember(String searchValue) {
		waitInSeconds(1);
		inputSearchValue(searchValue);
		clickSearchButton();
	}

	@Step("Verify if a user is displaying in the table on the Search page")
	public boolean isDisplayingUser(String userID, Member user){
		if(IDField.equals(userID)
				&& memberNameField.equals(user.getFirstName() + " " + user.getLastName())
				&& titleField.equals(user.getTitle())
				&& companyField.equals(user.getCompany())
				&& emailField.equals(user.getEmailAddress())){
			LOGGER.info("All data of the newly added member are displaying correctly on the Search page!");
			return true;
		}
		LOGGER.info("Not found member with ID: " +userID);
		return false;
	}

	@Step("Search Page is loaded")
	public boolean pageIsLoaded() {
		waitElementVisibility(searchTxt, 3);
		return searchTxt.isDisplayed();
	}
}
