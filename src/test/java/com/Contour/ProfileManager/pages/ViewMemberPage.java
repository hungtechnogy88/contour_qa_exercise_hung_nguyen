package com.Contour.ProfileManager.pages;

import com.Contour.ProfileManager.models.Member;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.logging.Logger;

public class ViewMemberPage extends NavigateBars {
	private static final Logger LOGGER = Logger.getLogger(ViewMemberPage.class.getName());

	@FindBy(id = "default-search")
	WebElement searchTxt;
	@FindBy(xpath = "//button[@type= \"submit\"]")
	WebElement searchButton;
	@FindBy(id = "first_name")
	WebElement firstNameTxt;
	@FindBy(id = "last_name")
	WebElement lastNameTxt;
	@FindBy(id = "title")
	WebElement titleTxt;
	@FindBy(id = "company")
	WebElement companyTxt;
	@FindBy(id = "phone")
	WebElement phoneTxt;
	@FindBy(id = "website")
	WebElement websiteTxt;
	@FindBy(id = "email")
	WebElement emailTxt;

	public ViewMemberPage(WebDriver driver) {
		super(driver);
	}

	@Step("Input Searching value")
	public void inputSearchValue(String searchValue) {
		searchTxt.clear();
		searchTxt.sendKeys(searchValue);
	}

	@Step("Click Search button")
	public void clickSearchButton() {
		searchButton.click();
	}

	@Step("Search a member")
	public void searchAMember(String searchValue) {
		inputSearchValue(searchValue);
		clickSearchButton();
	}

	@Step("Verify if a user is displaying in the table on the Search page")
	public boolean isDisplayingMember(Member user){
		if(firstNameTxt.getText().equals(user.getFirstName())
				&& lastNameTxt.getText().equals(user.getLastName())
				&& titleTxt.getText().equals(user.getTitle())
				&& companyTxt.getText().equals(user.getCompany())
				&& phoneTxt.getText().equals(user.getPhoneNumber())
				&& websiteTxt.getText().equals(user.getWebsiteURL())
				&& emailTxt.getText().equals(user.getEmailAddress())){
			return true;
		}
		return false;
	}

	@Step("View Member page is loaded")
	public boolean pageIsLoaded() {
		waitElementVisibility(searchTxt, 3);
		return searchTxt.isDisplayed();
	}
}
