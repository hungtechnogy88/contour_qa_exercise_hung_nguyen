package com.Contour.ProfileManager.pages;

import com.Contour.ProfileManager.models.Member;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.logging.Logger;

public class AddMemberPage extends NavigateBars {
	private static final Logger LOGGER = Logger.getLogger(AddMemberPage.class.getName());

	@FindBy(id = "first_name")
	WebElement firstNameTxt;
	@FindBy(id = "last_name")
	WebElement lastNameTxt;
	@FindBy(id = "title")
	WebElement titleTxt;
	@FindBy(id = "company")
	WebElement companyTxt;
	@FindBy(id = "phone")
	WebElement phoneTxt;
	@FindBy(id = "website")
	WebElement websiteTxt;
	@FindBy(id = "email")
	WebElement emailTxt;
	@FindBy(id = "remember")
	WebElement rememberCbx;
	@FindBy(xpath = "//button[@type = 'submit']")
	WebElement submitBtn;
	@FindBy(xpath = "//div[@class=\"ml-3 text-sm font-normal\"]")
	WebElement addSuccessMsg;

	public AddMemberPage(WebDriver driver) {
		super(driver);
	}

	@Step("Input First Name")
	public void inputFirstName(String fName) {
		firstNameTxt.clear();
		firstNameTxt.sendKeys(fName);
	}

	@Step("Input Last Name")
	public void inputLastName(String lName) {
		lastNameTxt.clear();
		lastNameTxt.sendKeys(lName);
	}

	@Step("Input Title")
	public void inputTitle(String title) {
		titleTxt.clear();
		titleTxt.sendKeys(title);
	}

	@Step("Input Company")
	public void inputCompany(String company) {
		companyTxt.clear();
		companyTxt.sendKeys(company);
	}

	@Step("Input Phone number")
	public void inputPhoneNumber(String phoneNumber) {
		phoneTxt.clear();
		phoneTxt.sendKeys(phoneNumber);
	}

	@Step("Input Website URL")
	public void inputWebsiteURL(String websiteURL) {
		websiteTxt.clear();
		websiteTxt.sendKeys(websiteURL);
	}

	@Step("Input Email address")
	public void inputEmailAddress(String emailAddress) {
		emailTxt.clear();
		emailTxt.sendKeys(emailAddress);
	}

	@Step("Check on Terms and conditions checkbox")
	public void checkOnTermsAndConditionsCheckbox() {
		if(!rememberCbx.isSelected()){
			rememberCbx.click();
		}
	}

	@Step("Uncheck on Terms and conditions checkbox")
	public void uncheckOnTermsAndConditionsCheckbox() {
		if(rememberCbx.isSelected()){
			rememberCbx.click();
		}
	}

	@Step("Click Submit button")
	public void clickSubmitButton() {
		submitBtn.click();
	}

	@Step("Verify If the Add Member Success message display")
	public boolean isAddMemberSuccessMessageDisplaying() {
		try {
			waitElementPresence(By.xpath("//div[@class=\"ml-3 text-sm font-normal\"]"), 1);
			return true;
		} catch (Exception exception) {
			return false;
		}
	}

	@Step("Get add member success message")
	public String getAddMemberSuccessMessage() {
		return addSuccessMsg.getText();
	}

	@Step("Get added member ID")
	public String getAddedMemberID() {
		String successMsg = getAddMemberSuccessMessage();
		return successMsg.substring(27, successMsg.length());
	}

	@Step("Add Member Page is loaded")
	public boolean pageIsLoaded() {
		waitElementVisibility(submitBtn, 3);
		return submitBtn.isDisplayed();
	}

	@Step("Add a new member")
	public void addANewMember(Member member) {
		inputFirstName(member.getFirstName());
		inputLastName(member.getLastName());
		inputTitle(member.getTitle());
		inputCompany(member.getCompany());
		inputPhoneNumber(member.getPhoneNumber());
		inputWebsiteURL(member.getWebsiteURL());
		inputEmailAddress(member.getEmailAddress());
		checkOnTermsAndConditionsCheckbox();
		clickSubmitButton();
	}

	@Step("Add a new member without check on Term And Condition checkbox")
	public void addANewMemberWithoutCheckOnTermAndCondition(Member member) {
		inputFirstName(member.getFirstName());
		inputLastName(member.getLastName());
		inputTitle(member.getTitle());
		inputCompany(member.getCompany());
		inputPhoneNumber(member.getPhoneNumber());
		inputWebsiteURL(member.getWebsiteURL());
		inputEmailAddress(member.getEmailAddress());
		uncheckOnTermsAndConditionsCheckbox();
		clickSubmitButton();
	}
}
