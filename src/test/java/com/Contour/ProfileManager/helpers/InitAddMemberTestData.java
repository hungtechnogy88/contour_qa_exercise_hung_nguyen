package com.Contour.ProfileManager.helpers;

import com.Contour.ProfileManager.models.Member;

public class InitAddMemberTestData {
    String memberPathFile = "src\\test\\resources\\testData\\Member.properties";

    public Member initDataForMemberWithEmptyFirstName(){
        Member emptyFName = new Member("",
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "lastName"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "title"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "company"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "phoneNumber"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "websiteURL"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "emailAddress"));
        return emptyFName;
    }

    public Member initDataForMemberWithEmptyLastName(){
        Member emptyLName = new Member(ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "firstName"),
                "",
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "title"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "company"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "phoneNumber"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "websiteURL"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "emailAddress"));
        return emptyLName;
    }

    public Member initDataForMemberWithEmptyTitle(){
        Member emptyTitle = new Member(ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "firstName"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "lastName"),
                "",
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "company"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "phoneNumber"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "websiteURL"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "emailAddress"));
        return emptyTitle;
    }

    public Member initDataForMemberWithEmptyCompany(){
        Member emptyCompany = new Member(ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "firstName"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "lastName"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "title"),
                "",
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "phoneNumber"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "websiteURL"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "emailAddress"));
        return emptyCompany;
    }

    public Member initDataForMemberWithEmptyEmail(){
        Member emptyEmail = new Member(ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "firstName"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "lastName"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "title"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "company"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "phoneNumber"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "websiteURL"),
                "");
        return emptyEmail;
    }

    public Member initDataForMemberWithInvalidEmailFormat(){
        Member invalidEmailFormat = new Member(ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "firstName"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "lastName"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "title"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "company"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "phoneNumber"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "websiteURL"),
                "abc.com");
        return invalidEmailFormat;
    }

    public Member initDataForMemberWithAllValidInfo(){
        Member ValidAllInfo = new Member(ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "firstName"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "lastName"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "title"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "company"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "phoneNumber"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "websiteURL"),
                ReadPropertiesFileHelper.readPropertiesFile(memberPathFile, "emailAddress"));
        return ValidAllInfo;
    }
}
