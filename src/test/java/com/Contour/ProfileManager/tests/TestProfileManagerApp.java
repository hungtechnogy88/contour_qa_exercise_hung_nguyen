package com.Contour.ProfileManager.tests;

import com.Contour.ProfileManager.helpers.InitAddMemberTestData;
import com.Contour.ProfileManager.models.Member;
import com.Contour.ProfileManager.pages.*;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;
import com.Contour.ProfileManager.helpers.ReadPropertiesFileHelper;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.asserts.SoftAssert;

public class TestProfileManagerApp {
    WebDriver driver;

    NavigateBars navigateBars;
    HomePage objHomePage;
    AddMemberPage objAddMemberPage;
    SearchMemberPage objSearchMemberPage;
    ViewMemberPage objViewMemberPage;
    String configPathFile = "src\\test\\resources\\testData\\Config.properties";
    InitAddMemberTestData initAddMemberTestData;
    Member memberWithEmptyFirstName;
    Member memberWithEmptyLastName;
    Member memberWithEmptyTitle;
    Member memberWithEmptyCompany;
    Member memberWithEmptyEmail;
    Member memberWithInvalidEmail;
    Member memberWithAllValidInfo;
    String addedMemberID;

    @BeforeTest
    public void beforeTest() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        driver = new ChromeDriver(options);
        driver.get(ReadPropertiesFileHelper.readPropertiesFile(configPathFile,"baseUrl"));
        driver.manage().window().maximize();
        initAddMemberTestData = new InitAddMemberTestData();
    }

    @Feature("Add member")
    @Test(priority = 0)
    @Description("Verify that user cannot add success a new member with First name is empty")
    public void addMemberWithEmptyFirstName(){
        objAddMemberPage = new AddMemberPage(driver);
        objAddMemberPage.navigateToAddMemberPage();
        Assert.assertTrue(objAddMemberPage.pageIsLoaded());
        memberWithEmptyFirstName = initAddMemberTestData.initDataForMemberWithEmptyFirstName();
        objAddMemberPage.addANewMember(memberWithEmptyFirstName);
        Assert.assertFalse(objAddMemberPage.isAddMemberSuccessMessageDisplaying(),
                "User still can add a new member with First name is empty");
    }

    @Feature("Add member")
    @Test(priority = 1)
    @Description("Verify that user cannot add success a new member with Last name is empty")
    public void addMemberWithEmptyLastName(){
        memberWithEmptyLastName = initAddMemberTestData.initDataForMemberWithEmptyLastName();
        objAddMemberPage.addANewMember(memberWithEmptyLastName);
        Assert.assertFalse(objAddMemberPage.isAddMemberSuccessMessageDisplaying(),
                "User still can add a new member with Last name is empty");
    }

    @Feature("Add member")
    @Test(priority = 2)
    @Description("Verify that user cannot add success a new member with Title is empty")
    public void addMemberWithEmptyTitle(){
        memberWithEmptyTitle = initAddMemberTestData.initDataForMemberWithEmptyTitle();
        objAddMemberPage.addANewMember(memberWithEmptyTitle);
        Assert.assertFalse(objAddMemberPage.isAddMemberSuccessMessageDisplaying(),
                "User still can add a new member with Title is empty");
    }

    @Feature("Add member")
    @Test(priority = 3)
    @Description("Verify that user cannot add success a new member with Company is empty")
    public void addMemberWithEmptyCompany(){
        memberWithEmptyCompany = initAddMemberTestData.initDataForMemberWithEmptyCompany();
        objAddMemberPage.addANewMember(memberWithEmptyCompany);
        Assert.assertFalse(objAddMemberPage.isAddMemberSuccessMessageDisplaying(),
                "User still can add a new member with Company is empty");
    }

    @Feature("Add member")
    @Test(priority = 4)
    @Description("Verify that user cannot add success a new member with Email address is empty")
    public void addMemberWithEmptyEmailAddress(){
        memberWithEmptyEmail = initAddMemberTestData.initDataForMemberWithEmptyEmail();
        objAddMemberPage.addANewMember(memberWithEmptyEmail);
        Assert.assertFalse(objAddMemberPage.isAddMemberSuccessMessageDisplaying(),
                "User still can add a new member with Email address is empty");
    }

    @Feature("Add member")
    @Test(priority = 5)
    @Description("Verify that user cannot add success a new member with invalid Email address")
    public void addMemberWithInvalidEmailAddress(){
        memberWithInvalidEmail = initAddMemberTestData.initDataForMemberWithInvalidEmailFormat();
        objAddMemberPage.addANewMember(memberWithInvalidEmail);
        Assert.assertFalse(objAddMemberPage.isAddMemberSuccessMessageDisplaying(),
                "User still can add a new member with invalid Email address");
    }

    @Feature("Add member")
    @Test(priority = 6)
    @Description("Verify that user cannot add success a new member without check on Term And Condition checkbox")
    public void addMemberWithoutCheckOnTermAndConditionCheckbox(){
        memberWithAllValidInfo = initAddMemberTestData.initDataForMemberWithAllValidInfo();
        objAddMemberPage.addANewMemberWithoutCheckOnTermAndCondition(memberWithAllValidInfo);
        Assert.assertFalse(objAddMemberPage.isAddMemberSuccessMessageDisplaying(),
                "User still can add a new member without check on Term And Condition checkbox");
    }

    @Feature("Add member")
    @Test(priority = 7)
    @Description("Verify that user can add success a new member with input all valid info")
    public void addMemberWithValidAllInfo(){
        memberWithAllValidInfo = initAddMemberTestData.initDataForMemberWithAllValidInfo();
        objAddMemberPage.addANewMember(memberWithAllValidInfo);
        Assert.assertTrue(objAddMemberPage.isAddMemberSuccessMessageDisplaying(),
                "User cannot add a new member with all valid info");
        Assert.assertTrue(objAddMemberPage.getAddMemberSuccessMessage().contains("Member has been added"));
        addedMemberID = objAddMemberPage.getAddedMemberID();
    }

    @Feature("Home page")
    @Test(priority = 8)
    @Description("Verify that added member is displaying on Home page")
    public void verifyAddedMemberOnHomePage(){
        objHomePage = new HomePage(driver);
        objHomePage.navigateToHomePage();
        Assert.assertTrue(objHomePage.isDisplayingUser(addedMemberID, memberWithAllValidInfo));
    }

    @Feature("Search page")
    @Test(priority = 9)
    @Description("Verify that added member is displaying on Search page")
    public void verifyAddedMemberOnSearchPage(){
        objSearchMemberPage = new SearchMemberPage(driver);
        objSearchMemberPage.navigateToSearchMemberPage();
        objSearchMemberPage.searchAMember(addedMemberID);
//        SoftAssert softAssert = new SoftAssert();
//        softAssert.assertTrue(objSearchMemberPage.isDisplayingUser(addedMemberID, memberWithAllValidInfo));
//        softAssert.assertAll();
    }

    @Feature("View Member page")
    @Test(priority = 10)
    @Description("Verify that added member is displaying on View Member page")
    public void verifyAddedMemberOnViewMemberPage(){
        objViewMemberPage = new ViewMemberPage(driver);
        objViewMemberPage.navigateToViewMemberPage();
        objViewMemberPage.searchAMember(addedMemberID);
        objViewMemberPage.isDisplayingMember(memberWithAllValidInfo);
    }

    @AfterTest
    public void afterTest() {
        driver.quit();
    }
}
